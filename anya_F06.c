#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <ctype.h>
#include <math.h>
#define ROT 13

static const char *animekuLogPath = "/home/afif/Wibu.log";
static const char *IANLogPath = "/home/afif/hayolongapain_f06.log";
static const char *directoryPath = "/home/afif/Downloads";
char prefix[9] = "Animeku_";
char prefix2[] = "IAN_";

void createLog(const char* old, char* new) {
	
	FILE * logFile = fopen(animekuLogPath, "a");
    fprintf(logFile, "%s --> %s\n", old, new);
    fclose(logFile);
}

void encode(char* strEnc1) { 
	if(strcmp(strEnc1, ".") == 0 || strcmp(strEnc1, "..") == 0)
        return;
    
    int strLength = strlen(strEnc1);
    for(int i = 0; i < strLength; i++) {
		if(strEnc1[i] == '/') 
            continue;
		if(strEnc1[i]=='.')
            break;
        
		if(strEnc1[i]>='A'&&strEnc1[i]<='Z')
            strEnc1[i] = 'Z'+'A'-strEnc1[i];
        if(strEnc1[i]>='a'&&strEnc1[i]<='z')
        {
            int j = (int)strEnc1[i];
            j = j + ROT;
            if(j > 122)
                j = j - 122 + 97 - 1;

            strEnc1[i] = j;            
        }
    }
}

void makeLogNo2a(char *commandName, char *before, char *after){
    char logMsg[1024];

    time_t currTime;
	struct tm *time_tm;
	time(&currTime);
	time_tm = localtime(&currTime);
    time_tm->tm_mon += 1;
    time_tm->tm_year += 1900;


	FILE *file;
	file = fopen(IANLogPath, "a");

	if (strcmp(commandName, "RMDIR") == 0 || strcmp(commandName, "UNLINK") == 0)
		sprintf(logMsg, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", time_tm->tm_mday, time_tm->tm_mon, time_tm->tm_year, time_tm->tm_hour, time_tm->tm_min, time_tm->tm_sec, commandName, before, after);

	else
		sprintf(logMsg, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s::%s\n", time_tm->tm_mday, time_tm->tm_mon, time_tm->tm_year, time_tm->tm_hour, time_tm->tm_min, time_tm->tm_sec, commandName, before, after);

	fputs(logMsg, file);
	fclose(file);
	return;

}

void makeLogNo2b(char *commandName, char *desc){
    char logMsg[1024];

    time_t currTime;
	struct tm *time_tm;
	time(&currTime);
	time_tm = localtime(&currTime);
    time_tm->tm_mon += 1;
    time_tm->tm_year += 1900;


	FILE *file;
	file = fopen(IANLogPath, "a");

	if (strcmp(commandName, "RMDIR") == 0 || strcmp(commandName, "UNLINK") == 0)
		sprintf(logMsg, "WARNING::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", time_tm->tm_mday, time_tm->tm_mon, time_tm->tm_year, time_tm->tm_hour, time_tm->tm_min, time_tm->tm_sec, commandName, desc);

	else
		sprintf(logMsg, "INFO::%.2d%.2d%d-%.2d:%.2d:%.2d::%s::%s\n", time_tm->tm_mday, time_tm->tm_mon, time_tm->tm_year, time_tm->tm_hour, time_tm->tm_min, time_tm->tm_sec, commandName, desc);

	fputs(logMsg, file);
	fclose(file);
	return;

}

void encodeVigenere(char* strEnc1) {
    if(strcmp(strEnc1, ".") == 0 || strcmp(strEnc1, "..") == 0)
        return;
    char key[] = "INNUGANTENG";
    int msgLen = strlen(strEnc1);
    int keyLen = strlen(key);
    char newKey[msgLen];
    char encryptedMsg[msgLen];
    int i, j;
    int s = 0;
    // generating new key
    for(i = 0, j = 0; i < msgLen; ++i, ++j){
        if(j == keyLen || strEnc1[i] == '/')
            j = 0;
 
        newKey[i] = key[j];
    }
 
    newKey[i] = '\0';
 
    //encryption
    for(i = 0, j = 0; i < msgLen; ++i, ++j){
        if('a' <= strEnc1[i] && 'z' >= strEnc1[i]){
            encryptedMsg[i] = tolower(((toupper(strEnc1[i]) + newKey[j]) % 26) + 'A');
            continue;
        }
        else if('A' <= strEnc1[i] && 'Z' >= strEnc1[i]){
            encryptedMsg[i] = ((strEnc1[i] + newKey[j]) % 26) + 'A';
            continue;
        }
        else
            j--;
    }

    encryptedMsg[i] = '\0';
    strcpy(strEnc1, encryptedMsg);
    printf("%s\n", encryptedMsg);
}

void decode(char * strDec1){
	if(strcmp(strDec1, ".") == 0 || strcmp(strDec1, "..") == 0 || strstr(strDec1, "/") == NULL) 
        return;
    
    int strLength = strlen(strDec1), s=0;
	for(int i = strLength; i >= 0; i--){
		if(strDec1[i]=='/')break;

		if(strDec1[i]=='.'){//nyari titik terakhir
			strLength = i;
			break;
		}
	}
	for(int i = 0; i < strLength; i++){
		if(strDec1[i]== '/'){
			s = i+1;
			break;
		}
	}
    for(int i = s; i < strLength; i++) {
		if(strDec1[i] =='/'){
            continue;
        }
        if(strDec1[i]>='A'&&strDec1[i]<='Z')
            strDec1[i] = 'Z'+'A'-strDec1[i];
        if(strDec1[i]>='a'&&strDec1[i]<='z')
        {
            int j = (int)strDec1[i];
            j = j + ROT;
            if(j > 122)
                j = j - 122 + 97 - 1;
            strDec1[i] = j;
        }
    }
	
}

void decodeVigenere(char *strEnc1){
    if(strcmp(strEnc1, ".") == 0 || strcmp(strEnc1, "..") == 0)
        return;
    char key[] = "INNUGANTENG";
    int msgLen = strlen(strEnc1);
    int keyLen = strlen(key);
    char newKey[keyLen];
    char decryptedMsg[msgLen];
    int i, j;
    // generating new key
    for(i = 0, j = 0; i < msgLen; ++i, ++j){
        if(j == keyLen)
            j = 0;
 
        newKey[i] = key[j];
    }
 
    newKey[i] = '\0';
 
    //encryption
    for(i = 0, j = 0; i < msgLen; ++i, ++j){
        if('a' <= strEnc1[i] && 'z' >= strEnc1[i]){
            decryptedMsg[i] = tolower(((toupper(strEnc1[i]) - newKey[j] + 26) % 26) + 'A');
            continue;
        }
        if('A' <= strEnc1[i] && 'Z' >= strEnc1[i]){
            decryptedMsg[i] = ((strEnc1[i] - newKey[j]) % 26) + 'A';
            continue;
        }
        else
            j--;
    }

    decryptedMsg[i] = '\0';
    printf("%s\n", decryptedMsg);
    strcpy(strEnc1, decryptedMsg);
}


static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    char *strEnc = strstr(path, prefix);
    if(strEnc != NULL) decode(strEnc);
    strEnc = strstr(path, prefix2);
    if(strEnc != NULL)
        decodeVigenere(strEnc);
    char newPath[1000];
    int res;
    sprintf(newPath, "%s %s", directoryPath, path);
    res = lstat(path, stbuf);
    
    if (res == -1) return -errno;
    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    char *strEnc = strstr(path, prefix);
    if(strEnc != NULL) decode(strEnc);
    char *strEnc2 = strstr(path, prefix2);
    if(strEnc2 != NULL)
        decodeVigenere(strEnc2);
    
    char newPath[1000];
    if(strcmp(path, "/") == 0){
    	path = directoryPath;
    	sprintf(newPath, "%s", path);
    }
    else{
    	sprintf(newPath, "%s %s", directoryPath, path);
    }

    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;
    int res = 0;

    dp = opendir(newPath);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(strEnc != NULL) encode(de->d_name);
        else if(strEnc2 != NULL)
            encode(de->d_name);

        res = (filler(buf, de->d_name, &st, 0));
        if(res!=0) break;
    }
    closedir(dp);
    return 0;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
    char newPath[1000];
    if(strcmp(path, "/") == 0){
    	path = directoryPath;
    	sprintf(newPath, "%s", path);
    }
    else{
    	sprintf(newPath, "%s %s", directoryPath, path);
    }
    
    int res = mkdir(newPath, mode);
    char str[100];
    sprintf(str, "Mkdir %s", path);
    
    char *folderPath = strstr(path, prefix);
    
    if(folderPath != NULL) {
        createLog(newPath, newPath);
        makeLogNo2b("MKDIR", newPath);
    }
    
    printf("%s\n", path);
    printf("%s\n", newPath);
    
    if (res == -1) res = -errno;

    return 0;
}

static int xmp_mknod(const char *path, mode_t mode, dev_t rdev)
{
    char newPath[1000];
    if(strcmp(path, "/") == 0){
    	path = directoryPath;
    	sprintf(newPath, "%s", path);
    }
    else{
    	sprintf(newPath, "%s %s", directoryPath, path);
    }
    
    int res;
    
    if(S_ISREG(mode)){
    	res = open(newPath, O_CREAT | O_EXCL | O_WRONLY, mode);
    	if(res >= 0)
    		res = close(res);
    }
    else if (S_ISFIFO(mode)){
    	res = mkfifo(newPath, mode);
    }
    else{
    	res = mknod(newPath, mode, rdev);
    }
    
    char str[100];
    sprintf(str, "Create %s", path);
    
    if (res == -1) res = -errno;

    return 0;
}

static int xmp_unlink(const char *path)
{
    char *strEnc = strstr(path, prefix);
    char *strEnc2 = strstr(path, prefix2);
    if (strEnc != NULL)
        decode(strEnc);
    else if(strEnc2 != NULL)
        decode(strEnc2);

    char newPath[1000];
    if(strcmp(path, "/") == 0){
    	path = directoryPath;
    	sprintf(newPath, "%s", path);
    }
    else{
    	sprintf(newPath, "%s %s", directoryPath, path);
    }
    
    char str[100];
    sprintf(str, "Remove %s", path);
    
    char *folderPath = strstr(path, prefix);
    
    int res;
    res = unlink(newPath);
    if (res == -1) res = -errno;

    makeLogNo2b("UNLINK", newPath);
    return 0;
}

static int xmp_rmdir(const char *path)
{
    char *strEnc = strstr(path, prefix);
    char *strEnc2 = strstr(path, prefix2);
    if (strEnc != NULL)
        decode(strEnc);
    else if(strEnc2 != NULL){
        decode(strEnc2);
    }

    char newPath[1000];
    sprintf(newPath, "%s %s", directoryPath, path);
    char str[100];
    sprintf(str, "Rmdir %s", path);
    
    int res;
    res = rmdir(newPath);
    
    if (res == -1) res = -errno;

    makeLogNo2b("RMDIR", newPath);

    return 0;
}

static int xmp_rename(const char *source, const char *dest)
{
    char fileSource[1000], fileDest[1000];
    sprintf(fileSource, "%s %s", directoryPath, source);
    sprintf(fileDest, "%s %s", directoryPath, dest);
    
    char str[100];
    sprintf(str, "Rename %s -> %s", source, dest);
    
    createLog(fileSource, fileDest);
    makeLogNo2a("RENAME", fileSource, fileDest);
    int res;
    res = rename(fileSource, fileDest);
    
    if (res == -1) res = -errno;

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi)
{
    int res;
    
    char newPath[1000];
    sprintf(newPath, "%s %s", directoryPath, path);
    
    res = open(newPath, fi->flags);
    
    if (res == -1) res = -errno;

    close(res);

    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void) fi;
    
    char newPath[1000];
    sprintf(newPath, "%s %s", directoryPath, path);
    
    fd = open(path, O_RDONLY);
    
    if (fd == -1) return -errno;
        
    res = pread(fd, buf, size, offset);
    
    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void) fi;
    
    char newPath[1000];
    sprintf(newPath, "%s %s", directoryPath, path);
    
    fd = open(path, O_RDONLY);
    
    if (fd == -1) return -errno;
    
    char str[100];
    sprintf(str, "Write = %s", path);
    
    res = pwrite(fd, buf, size, offset);
    
    if (res == -1) res = -errno;

    close(fd);

    return res;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
	.readdir = xmp_readdir,
	.read = xmp_read,
	.mkdir = xmp_mkdir,
	.mknod = xmp_mknod,
	.unlink = xmp_unlink,
	.rmdir = xmp_rmdir,
	.rename = xmp_rename,
	.open = xmp_open,
	.write = xmp_write,

};



int  main(int  argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
